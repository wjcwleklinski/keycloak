FROM quay.io/keycloak/keycloak:18.0.1 as builder

ENV KC_HEALTH_ENABLED=true \
KC_METRICS_ENABLED=true \
KC_FEATURES=token-exchange \
KC_DB=postgres

# Install custom providers
RUN curl -sL https://github.com/aerogear/keycloak-metrics-spi/releases/download/2.5.3/keycloak-metrics-spi-2.5.3.jar -o /opt/keycloak/providers/keycloak-metrics-spi-2.5.3.jar
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:18.0.1
COPY --from=builder /opt/keycloak/ /opt/keycloak/
WORKDIR /opt/keycloak
# for demonstration purposes only, please make sure to use proper certificates in production instead
#RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore

ENV KC_DB_URL=jdbc:postgresql://postgres-keycloak:5432/keycloak \
KC_DB_USERNAME=keycloak \
KC_DB_PASSWORD=keycloak \
KC_DB_PORT=5432 \
KC_HOSTNAME=localhost \
KEYCLOAK_IMPORT=/opt/keycloak/data/import/checklist-realm.json \
KEYCLOAK_ADMIN=admin \
KEYCLOAK_ADMIN_PASSWORD=admin
ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--import-realm"]